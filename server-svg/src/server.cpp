#include "server.h"


//#include "engine.h"
//#include "myserver.h"
//#include "tcp-server.h"
#include "client-delegates.h"
#include "abstract-data-engine.h"
#include <QDebug>



// Конструктор
ServerSVG::ServerSVG(QObject *parent)
    : QObject (parent)
    , serverSVG_(Q_NULLPTR)
{

}

// Деструктор
ServerSVG::~ServerSVG()
{

}

// Инициализатор
void ServerSVG::initServerSVG()
{
    //
    serverSVG_ = new TcpServer();

//    serverSVG_->setEngineDefiner(new Engine());

    // Обработка подключения клиентов
    connect(serverSVG_, &TcpServer::clientAuthorized,
            this, &ServerSVG::clientAutorized, Qt::AutoConnection);

    // Обработка отключения клиентов
    connect(serverSVG_, &TcpServer::clientAboutToDisconnect,
            this, &ServerSVG::clientDisconnected, Qt::AutoConnection);

    serverSVG_->start(1992u);

    if (serverSVG_->isListening())
    {
        qDebug() << "Yes!";
    }
    else
    {
        qDebug() << "No!";
    }

}

void ServerSVG::clientAutorized(ClientFace* client)
{

}

void ServerSVG::clientDisconnected(ClientFace* client)
{

}



