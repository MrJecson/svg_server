#include <QCoreApplication>
#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ServerSVG serverSVG;
    serverSVG.initServerSVG();

    return a.exec();
}
