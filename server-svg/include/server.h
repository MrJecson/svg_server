#ifndef SERVER_H
#define SERVER_H

#include "clients-data.h"

// Класс сервер SVG
class ServerSVG Q_DECL_FINAL : public QObject
{

    Q_OBJECT

public:

    ServerSVG(QObject* parent = Q_NULLPTR);
    ~ServerSVG();

    /// Экземпляр класса ЭД
    TcpServer* serverSVG_;

    /// Экземпляр структуры клиентов ЭД
    clients_data_t clients_;

    /// Инициализатор Сервера!
    void initServerSVG();

private slots:

    /// Слот обратотки подключения клиента
    void clientAutorized(ClientFace* client);

    /// Слот обработки отключения клиента
    void clientDisconnected(ClientFace* client);
};


#endif // SERVER_H
